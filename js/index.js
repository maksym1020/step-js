"use strict";

const DATA = [
    {
        "first name": "Олексій",
        "last name": "Петров",
        photo: "./img/trainers/trainer-m1.jpg",
        specialization: "Басейн",
        category: "майстер",
        experience: "8 років",
        description:
            "Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
    },
    {
        "first name": "Марина",
        "last name": "Іванова",
        photo: "./img/trainers/trainer-f1.png",
        specialization: "Тренажерний зал",
        category: "спеціаліст",
        experience: "2 роки",
        description:
            "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
    },
    {
        "first name": "Ігор",
        "last name": "Сидоренко",
        photo: "./img/trainers/trainer-m2.jpg",
        specialization: "Дитячий клуб",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
    },
    {
        "first name": "Тетяна",
        "last name": "Мороз",
        photo: "./img/trainers/trainer-f2.jpg",
        specialization: "Бійцівський клуб",
        category: "майстер",
        experience: "10 років",
        description:
            "Тетяна є експертом в бойових мистецтвах. Вона проводить тренування для професіоналів і новачків. Її підхід до навчання допомагає спортсменам досягати високих результатів.",
    },
    {
        "first name": "Сергій",
        "last name": "Коваленко",
        photo: "./img/trainers/trainer-m3.jpg",
        specialization: "Тренажерний зал",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Сергій фокусується на роботі з фітнесом та кардіотренуваннями. Він вдосконалив свої методики протягом багатьох років. Його учні завжди в формі та енергійні.",
    },
    {
        "first name": "Олена",
        "last name": "Лисенко",
        photo: "./img/trainers/trainer-f3.jpg",
        specialization: "Басейн",
        category: "спеціаліст",
        experience: "4 роки",
        description:
            "Олена спеціалізується на синхронному плаванні. Вона тренує команди різного рівня. Її команди завжди займають призові місця на змаганнях.",
    },
    {
        "first name": "Андрій",
        "last name": "Волков",
        photo: "./img/trainers/trainer-m4.jpg",
        specialization: "Бійцівський клуб",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Андрій має досвід у вивченні різних бойових мистецтв. Він викладає техніку та тактику бою. Його учні здобувають перемоги на міжнародних турнірах.",
    },
    {
        "first name": "Наталія",
        "last name": "Романенко",
        photo: "./img/trainers/trainer-f4.jpg",
        specialization: "Дитячий клуб",
        category: "спеціаліст",
        experience: "3 роки",
        description:
            "Наталія розробила унікальну програму для дітей дошкільного віку. Вона допомагає дітям розвивати фізичні та ментальні навички. Її класи завжди веселі та динамічні.",
    },
    {
        "first name": "Віталій",
        "last name": "Козлов",
        photo: "./img/trainers/trainer-m5.jpg",
        specialization: "Тренажерний зал",
        category: "майстер",
        experience: "10 років",
        description:
            "Віталій спеціалізується на функціональному тренуванні. Він розробив ряд ефективних тренувальних програм. Його клієнти швидко досягають бажаних результатів.",
    },
    {
        "first name": "Юлія",
        "last name": "Кравченко",
        photo: "./img/trainers/trainer-f5.jpg",
        specialization: "Басейн",
        category: "спеціаліст",
        experience: "4 роки",
        description:
            "Юлія є експертом у водних видах спорту. Вона проводить тренування з аквагімнастики та аеробіки. Її учні демонструють вражаючі показники на змаганнях.",
    },
    {
        "first name": "Олег",
        "last name": "Мельник",
        photo: "./img/trainers/trainer-m6.jpg",
        specialization: "Бійцівський клуб",
        category: "майстер",
        experience: "12 років",
        description:
            "Олег є визнаним майстром в бойових мистецтвах. Він тренує чемпіонів різних вагових категорій. Його методики вважаються одними з найефективніших у світі бойових мистецтв.",
    },
    {
        "first name": "Лідія",
        "last name": "Попова",
        photo: "./img/trainers/trainer-f6.jpg",
        specialization: "Дитячий клуб",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Лідія має великий досвід у роботі з дітьми. Вона організовує різноманітні спортивні ігри та заняття. Її класи завжди допомагають дітям розвивати соціальні навички та командний дух.",
    },
    {
        "first name": "Роман",
        "last name": "Семенов",
        photo: "./img/trainers/trainer-m7.jpg",
        specialization: "Тренажерний зал",
        category: "спеціаліст",
        experience: "2 роки",
        description:
            "Роман є експертом у кросфіту та функціональних тренуваннях. Він розробив власні програми для різних вікових груп. Його учні часто отримують нагороди на фітнес-турнірах.",
    },
    {
        "first name": "Анастасія",
        "last name": "Гончарова",
        photo: "./img/trainers/trainer-f7.jpg",
        specialization: "Басейн",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Анастасія фокусується на водних програмах для здоров'я та фітнесу. Вона проводить тренування для осіб з різним рівнем підготовки. Її учні відзначають покращення здоров'я та благополуччя після занять.",
    },
    {
        "first name": "Валентин",
        "last name": "Ткаченко",
        photo: "./img/trainers/trainer-m8.jpg",
        specialization: "Бійцівський клуб",
        category: "спеціаліст",
        experience: "2 роки",
        description:
            "Валентин є експертом в таеквондо та кікбоксингу. Він викладає техніку, тактику та стратегію бою. Його учні часто стають чемпіонами на національних та міжнародних змаганнях.",
    },
    {
        "first name": "Лариса",
        "last name": "Петренко",
        photo: "./img/trainers/trainer-f8.jpg",
        specialization: "Дитячий клуб",
        category: "майстер",
        experience: "7 років",
        description:
            "Лариса розробила комплексну програму для розвитку фізичних та інтелектуальних навичок дітей. Вона проводить заняття в ігровій формі. Її методика допомагає дітям стати активними та розумними.",
    },
    {
        "first name": "Олексій",
        "last name": "Петров",
        photo: "./img/trainers/trainer-m9.jpg",
        specialization: "Басейн",
        category: "майстер",
        experience: "11 років",
        description:
            "Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
    },
    {
        "first name": "Марина",
        "last name": "Іванова",
        photo: "./img/trainers/trainer-f9.jpg",
        specialization: "Тренажерний зал",
        category: "спеціаліст",
        experience: "2 роки",
        description:
            "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
    },
    {
        "first name": "Ігор",
        "last name": "Сидоренко",
        photo: "./img/trainers/trainer-m10.jpg",
        specialization: "Дитячий клуб",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
    },
    {
        "first name": "Наталія",
        "last name": "Бондаренко",
        photo: "./img/trainers/trainer-f10.jpg",
        specialization: "Бійцівський клуб",
        category: "майстер",
        experience: "8 років",
        description:
            "Наталія є майстром у бойових мистецтвах. Вона вивчала різні техніки та стили, із якими працює зі своїми учнями. Її підхід до навчання відповідає найвищим стандартам.",
    },
    {
        "first name": "Андрій",
        "last name": "Семенов",
        photo: "./img/trainers/trainer-m11.jpg",
        specialization: "Тренажерний зал",
        category: "інструктор",
        experience: "1 рік",
        description:
            "Андрій спеціалізується на функціональному тренуванні. Він розробив власну систему вправ для зміцнення корпусу. Його учні завжди отримують видимі результати.",
    },
    {
        "first name": "Софія",
        "last name": "Мельник",
        photo: "./img/trainers/trainer-f11.jpg",
        specialization: "Басейн",
        category: "спеціаліст",
        experience: "6 років",
        description:
            "Софія працює з аквагімнастикою. Вона вивчила різні техніки та стили плавання. Її заняття допомагають клієнтам розслабитися та покращити фізичну форму.",
    },
    {
        "first name": "Дмитро",
        "last name": "Ковальчук",
        photo: "./img/trainers/trainer-m12.png",
        specialization: "Дитячий клуб",
        category: "майстер",
        experience: "10 років",
        description:
            "Дмитро спеціалізується на розвитку дитячого спорту. Він розробив унікальну програму для малюків. Його методики забезпечують гармонійний розвиток дітей.",
    },
    {
        "first name": "Олена",
        "last name": "Ткаченко",
        photo: "./img/trainers/trainer-f12.jpg",
        specialization: "Бійцівський клуб",
        category: "спеціаліст",
        experience: "5 років",
        description:
            "Олена є відомим тренером у жіночому бойовому клубі. Вона вивчила різні техніки самооборони. Її підхід дозволяє її ученицям відчувати себе впевнено в будь-яких ситуаціях.",
    },
];

const fragment = document.createDocumentFragment();

// Клонування вихідного масиву, щоб не модифікувати вихідний масив
const trainers = Object.assign(DATA);
for (let i = 0; i < trainers.length; i++) {

    // Додано ключ id для гарантованих результатів фільтрації (через повтори імен та фамілій в масиві)
    trainers[i].id = i;
}


const trainerList = document.querySelector('.trainers-cards__container');

// Функція виводу карток тренерів на сторінку
// Функція приймає масив як аргумент на той випадок, якщо після фільтрації потрібно саме видаляти тренерів зі сторінки
// Якщо це буде потрібно, можна буде викликати далі в коді (повинна спрацювати)
function displayTrainerItems(array) {

    const trainerTemplate = document.querySelector('#trainer-card');

    for (let i = 0; i < array.length; i++) {

        // Клонування та підготовка картки для подальшого заповнення
        const trainerTemplateClone = trainerTemplate.content.cloneNode(true);
        const trainerListItem = trainerTemplateClone.querySelector('.trainer');

        // Змінні картки тренера для подальшого заповнення
        const trainerName = trainerListItem.children[1];
        const trainerPhoto = trainerListItem.children[0];
        const experienceYears = array[i]["experience"].split(' ')[0];

        // Додавання ім'я та прізвища до картки тренера
        trainerName.textContent = `${array[i]["first name"]} ${array[i]["last name"]}`;

        // Додавання фото до картки тренера
        trainerPhoto.src = array[i].photo;

        // Додавання id за збільшенням до кожної картки для подальшого сортування за замовчанням
        trainerListItem.id = i;

        // Додавання років досвіду тренера для подальшого сортування
        trainerListItem.dataset.experience = experienceYears;

        fragment.append(trainerListItem);
    }
    trainerList.append(fragment)
}

displayTrainerItems(trainers);


// Вивід блоку сортування на сторінку
const sortingSection = document.querySelector('.sorting');
sortingSection.hidden = '';

// Вивід блоку фільтрації на сторінку
const sidebarSection = document.querySelector('.sidebar');
sidebarSection.hidden = '';


// Пошук шаблону модального вікна, клонування та підготовка до заповнення
const modalTemplate = document.querySelector('#modal-template');
const modalTemplateClone = modalTemplate.content.cloneNode(true);
const modal = modalTemplateClone.querySelector('.modal');


// Виклик модального вікна по кліку на будь-якій з карток тренерів
trainerList.addEventListener('click', onClickShowModal);


// Функція відображення модального вікна
function onClickShowModal(event) {

    // Змінні модального вікна, що потрібно заповнити
    const trainerPhoto = modal.querySelector('.modal__img');
    const trainerName = modal.querySelector('.modal__name');
    const trainerCategory = modal.querySelector('.modal__point--category');
    const trainerExperience = modal.querySelector('.modal__point--experience');
    const trainerSpecialization = modal.querySelector('.modal__point--specialization');
    const trainerDescription = modal.querySelector('.modal__text');

    if (event.target.type === 'button') {

        // ID активованого елемента, для котрого потрібно вивести додаткову інформацію
        const ID = +event.target.closest('li').id;

        // Функція заповнення модального вікна інформацією про тренера
        function fillModal(array) {
            trainerPhoto.src = array[ID].photo;
            trainerName.textContent = `${array[ID]["first name"]} ${array[ID]["last name"]}`;
            trainerCategory.textContent = `Категорія: ${array[ID].category}`;
            trainerExperience.textContent = `Досвід: ${array[ID].experience}`;
            trainerSpecialization.textContent = `Напрям тренера: ${array[ID].specialization}`;
            trainerDescription.textContent = array[ID].description;
        }

        fillModal(trainers);

        document.body.append(modal);

        // Блокування прокрутки сторінки при відкритті модального вікна
        modal.addEventListener('wheel', (e) => e.preventDefault());
    }
}

// Видалення модального вікна
modal.addEventListener('click', onClickRemoveModal);

// Функція видалення модального вікна по кліку на кнопку "Закрити"
function onClickRemoveModal(event) {
    if (event.target.closest('button')) {
        modal.remove();
    }
}


// Список всіх карток тренерів
const trainerCards = [...trainerList.querySelectorAll('.trainer')];

// Виклик сортування карток тренерів при кліку на кнопці сортування
sortingSection.addEventListener('click', onButtonClickSortCards);

// Функція сортування карток тренерів
function onButtonClickSortCards(event) {

    // Видалення попередніх та підсвічування активованих кнопок сортування
    if (event.target.closest('button')) {

        const sortingButtons = sortingSection.querySelectorAll('.sorting__btn');

        // Видалення підсвічування попередньо активованих кнопок
        sortingButtons.forEach((value) => value.classList.remove('sorting__btn--active'));

        // Підсвічування активованої кнопки сортування
        event.target.closest('button').classList.add('sorting__btn--active');
    }

    // Сортування карток за замовчанням
    if (event.target.closest('button') &&
        event.target.closest('button').textContent.trim() === 'ЗА замовчанням') {

        trainerCards.sort((a, b) => a.id - b.id);
    }

    // Сортування карток за прізвищем та ім'ям
    if (event.target.closest('button') &&
        event.target.closest('button').textContent.trim() === 'ЗА ПРІЗВИЩЕМ') {

        trainerCards.sort((a, b) => {

            let firstNameA = a.children[1].textContent.split(' ')[0];
            let firstNameB = b.children[1].textContent.split(' ')[0];

            let lastNameA = a.children[1].textContent.split(' ')[1];
            let lastNameB = b.children[1].textContent.split(' ')[1];

            return lastNameA === lastNameB ? firstNameA.localeCompare(firstNameB) : lastNameA.localeCompare(lastNameB);

        });
    }

    // Сортування карток за досвідом тренера від найбільшого
    if (event.target.closest('button') &&
        event.target.closest('button').textContent.trim() === 'ЗА ДОСВІДОМ') {

        trainerCards.sort((a, b) => b.dataset.experience - a.dataset.experience);
    }

    fragment.append(...trainerCards);
    trainerList.append(fragment);
}


// Форма фільтрації в боковому меню
const filterForm = document.querySelector('.sidebar__filters');

// Виклик функції фільтрації
filterForm.addEventListener('submit', onSubmitFilterCards);


function onSubmitFilterCards(event) {

    // Блокування перезавантаження сторінки
    event.preventDefault();

    // Клонування вихідного масиву тренерів для подальшої фільтрації
    let trainersClone = Object.assign(trainers);

    // Виклик всіх інпутів та лейблів, що використовуються в боковому меню
    const filterFormInputs = [...document.querySelectorAll('.filters__input')];
    const filterFormLabels = [...document.querySelectorAll('.filters__label')];

    // Пошук всіх обраних інпутів
    const filteredInputs = filterFormInputs.filter((value) => value.checked === true);


    // Фільтрація за напрямом
    const trainerArea = filterFormLabels.find((element) =>
        element.htmlFor === filteredInputs[0].id).textContent;

    if (trainerArea.trim() !== 'ВСІ') {
        trainersClone = trainersClone.filter((value) =>
            value.specialization.trim().toLowerCase() === trainerArea.trim().toLowerCase());
    }


    // Фільтрація за категорією
    const trainerCategory = filterFormLabels.find((element) =>
        element.htmlFor === filteredInputs[1].id).textContent;


    if (trainerCategory.trim() !== 'ВСІ') {
        trainersClone = trainersClone.filter((value) =>
            value.category.trim().toLowerCase() === trainerCategory.trim().toLowerCase());
    }

    // Приховування всіх карток
    trainerCards.forEach((value) => value.style = 'display: none;')

    // Відображення тих карток, що відповідають обраним селектам в формі фільтрації
    trainerCards.forEach((value) => {
        trainersClone.find((element) => {
            if (+element.id === +value.id) {
                value.style = 'display: flex;';
            }
        })
    })
}
